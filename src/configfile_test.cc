/*******************************************************************************
********************************************************************************

        Copyright (c) 2008 Ahmed S. Badran

          Filename:  configfile_test.cc
       Description:  Testing the config file.
           Created:  08/15/2008 11:43:24 PM PDT
          Revision:  $Rev$
            Author:  Ahmed S. Badran (Ahmed B.), ahmed.badran@gmail.com

********************************************************************************
*******************************************************************************/
#include "configfile.h"
int main()
{
	configfile conf("config_file.txt");
	conf.add_section("option.test");
	conf.add_key("option.test", "key", "value");
	conf.add_key("core", "core-key", "core key value");
	conf.save();
	return 0;
}
