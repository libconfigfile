/*******************************************************************************
********************************************************************************

        Copyright (c) 2008 Ahmed S. Badran

          Filename:  configfile.cc
       Description:  Implementation.
           Created:  08/12/2008 10:27:53 PM PDT
          Revision:  $Rev$
            Author:  Ahmed S. Badran (Ahmed B.), ahmed.badran@gmail.com

********************************************************************************
*******************************************************************************/
#include "configfile.h"
using namespace std;

configfile::configfile(const string& fpath):
	file_path(fpath)
{
	unsigned MAX = 4096;
	char* buf = new char[MAX];
	FILE* filp = fopen(file_path.c_str(), "r");
	if (!filp)
		return;
	string secname = "", key = "", value = "";
	while (getline(&buf, &MAX, filp) != -1) {
		if (is_comment(buf)) {
			/* drop it */
		} else if (is_section_header(buf, &secname)) {
			add_section(secname);
		} else if (is_key_value(buf, &key, &value)) {
			if (secname != "") {
				add_key(secname, key, value);
			}
		} else {
			/* drop it too */
		}
		memset(buf, 0, MAX);
	}
	fclose(filp);
	filp = 0;
}

vector<string>
configfile::get_sections () const
{
	vector<string> section_names;
	data_struct::const_iterator pos;
	data_struct::const_iterator end = sections.end();
	for (pos = sections.begin(); pos != end; ++pos) {
		section_names.push_back(pos->first);
	}
	return section_names;
}

vector<string>
configfile::get_keys (const string& secname) const
{
	unsigned index = 0;
	vector<string> keys;
	if (!get_section_index(secname, &index)) {
		return keys;
	}
	const vector<pair<string, string> >& key_values =
						sections[index].second;
	vector<pair<string, string> >::const_iterator pos;
	vector<pair<string, string> >::const_iterator end = key_values.end();
	for (pos = key_values.begin(); pos != end; ++pos) {
		keys.push_back(pos->first);
	}
	return keys;
}

bool
configfile::get_key_value (const string& secname, const string& key, string*
			   value) const
{
	unsigned index = 0;
	if (!get_section_index(secname, &index)) {
		return false;
	}
	const vector<pair<string, string> >& key_values =
						sections[index].second;
	vector<pair<string, string> >::const_iterator pos;
	vector<pair<string, string> >::const_iterator end = key_values.end();
	for (pos = key_values.begin(); pos != end; ++pos) {
		if (pos->first == key) {
			*value = pos->second;
			return true;
		}
	}
	return false;
}

void
configfile::add_section (const string& name)
{
	vector<pair<string, string> > empty;
	sections.push_back(make_pair(name, empty));
}

bool
configfile::add_key (const string& secname, const string& key, const string&
		     value)
{
	unsigned index = 0;
	if (!get_section_index(secname, &index)) {
		return false;
	}
	vector<pair<string, string> >& key_values = sections[index].second;
	key_values.push_back(make_pair(key, value));
	return true;
}

bool
configfile::get_section_index (const string& secname, unsigned* index) const
{
	unsigned i = 0;
	unsigned section_count = sections.size();
	for (i = 0; i < section_count; ++i) {
		if (sections[i].first == secname) {
			*index = i;
			return true;
		}
	}
	return false;
}

bool
configfile::is_comment (const char* buf) const
{
	bool retval;
	regex_t regex;
	regcomp(&regex, "^[ 	]*#.*$", REG_EXTENDED);
	if (regexec(&regex, buf, 0, 0, 0) == 0) {
		retval = true;
	} else {
		retval = false;
	}
	regfree(&regex);
	return retval;
}
bool
configfile::is_section_header(const char* buf, string* secname) const
{
	const unsigned MAX = 1024;
	regmatch_t substr[3];
	char tmpbuf[MAX] = {0};
	memset(substr, 0, sizeof(substr));
	regex_t regex;
	regcomp(&regex, "^[ 	]*\\[[ 	]*([^ 	]+)[ 	]+\"([^ 	]+)\""
		"[ 	]*\\][ 	]*$", REG_EXTENDED | REG_NEWLINE);
	if (regexec(&regex, buf, 3, substr, 0) == 0) {
		memset(tmpbuf, 0, MAX);
		extract_field(1, substr, buf, tmpbuf);
		*secname = tmpbuf;
		*secname += ".";
		memset(tmpbuf, 0, MAX);
		extract_field(2, substr, buf, tmpbuf);
		*secname += tmpbuf;
		regfree(&regex);
		return true;
	}

	regcomp(&regex, "^[ 	]*\\[[ 	]*([^ 	]+)[ 	]*\\][ 	]*$",
		REG_EXTENDED | REG_NEWLINE);
	if (regexec(&regex, buf, 3, substr, 0) == 0) {
		memset(tmpbuf, 0, MAX);
		extract_field(1, substr, buf, tmpbuf);
		*secname = tmpbuf;
		regfree(&regex);
		return true;
	}
	return false;
}

bool
configfile::is_key_value(const char* buf, string* key, string* value) const
{
	const unsigned MAX = 1024;
	regmatch_t substr[3];
	char tmpbuf[MAX] = {0};
	memset(substr, 0, sizeof(substr));
	regex_t regex;
	regcomp(&regex, "^[ 	]*([^ 	].*[^ 	])[ 	]*=[ 	]*"
		"([^ 	].*[^ 	])[ 	]*$", REG_EXTENDED | REG_NEWLINE);
	if (regexec(&regex, buf, 3, substr, 0) == 0) {
		memset(tmpbuf, 0, MAX);
		extract_field(1, substr, buf, tmpbuf);
		*key = tmpbuf;
		memset(tmpbuf, 0, MAX);
		extract_field(2, substr, buf, tmpbuf);
		*value = tmpbuf;
		return true;
	}
	return false;
}

void
configfile::extract_field (unsigned i, regmatch_t* regex_arr, const char*
			   inbuf, char* outbuf) const
{
	strncpy(outbuf, &inbuf[regex_arr[i].rm_so], regex_arr[i].rm_eo -
		regex_arr[i].rm_so);
}

bool
configfile::save_to (const string& fpath) const
{
	FILE* filp = fopen(fpath.c_str(), "w");
	if (!filp) {
		return false;
	}
	data_struct::const_iterator pos;
	data_struct::const_iterator end = sections.end();
	string tmp1, tmp2;
	unsigned index = 0;
	for (pos = sections.begin(); pos != end; ++pos) {
		index = pos->first.find('.', 0);
		if (index != string::npos) {
			tmp1 = pos->first.substr(0, index);
			tmp2 = pos->first.substr(index + 1);
			fprintf(filp, "[%s \"%s\"]\n", tmp1.c_str(),
				tmp2.c_str());
		} else {
			fprintf(filp, "[%s]\n", pos->first.c_str());
		}
		const vector<pair<string, string> >& key_values = pos->second;
		vector<pair<string, string> >::const_iterator kpos;
		vector<pair<string, string> >::const_iterator kend =
							key_values.end();
		for (kpos = key_values.begin(); kpos != kend; ++kpos) {
			fprintf(filp, "\t%s = %s\n", kpos->first.c_str(),
				kpos->second.c_str());
		}
	}
	fclose(filp);
	return true;
}

bool
configfile::save ()
{
	return save_to(file_path);
}

configfile::~configfile ()
{
}

