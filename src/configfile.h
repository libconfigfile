/*******************************************************************************
********************************************************************************

        Copyright (c) 2008 Ahmed S. Badran

          Filename:  configfile.h
       Description:  A simple ini-like configuration file abstraction.
           Created:  08/12/2008 10:19:01 PM PDT
          Revision:  $Rev$
            Author:  Ahmed S. Badran (Ahmed B.), ahmed.badran@gmail.com

********************************************************************************
*******************************************************************************/
#ifndef  CONFIGFILE_INC
#define  CONFIGFILE_INC
#include <string>
#include <vector>
#include <utility>
#include <cstdio>
#include <sys/types.h>
#include <regex.h>

class configfile
{
	public:
	configfile(const std::string& fpath);
	std::vector<std::string> get_sections() const;
	std::vector<std::string> get_keys(const std::string& secname) const;
	bool get_key_value(const std::string& secname, const std::string& key,
			   std::string* value) const;
	void add_section(const std::string& name);
	bool add_key(const std::string& secname, const std::string& key, const
		     std::string& value);
	bool save();
	bool save_to(const std::string& fpath) const;
	~configfile();

	private:
	bool get_section_index(const std::string& secname, unsigned* index)
		const;
	bool is_comment(const char*) const;
	bool is_section_header(const char*, std::string* secname) const;
	bool is_key_value(const char*, std::string* key, std::string* value)
		const;
	void extract_field(unsigned i, regmatch_t* regex_arr, const char*
			   inbuf, char* outbuf) const;
	private:
	const std::string file_path;
	typedef std::vector< std::pair<std::string,
		std::vector<std::pair<std::string, std::string> > > >
			data_struct;
	data_struct sections;
};
#endif   // ----- #ifndef CONFIGFILE_INC  -----
